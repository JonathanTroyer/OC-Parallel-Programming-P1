#include <stdio.h>
#include <memory.h>
#include <omp.h>

int Find_bin(float val, const float bin_maxes[], const int bin_count, float min_meas) {
    if (min_meas <= val && val < bin_maxes[0]) {
        return 0;
    }
    for (int i = 1; i < bin_count; i++) {
        if (bin_maxes[i - 1] <= val && val < bin_maxes[i]) {
            return i;
        }
    }
}

int main() {
    //Setup data info
    float data[] = {1.3, 2.9, 0.4, 0.3, 1.3, 4.4, 1.7, 0.4, 3.2, 0.3, 4.9, 2.4, 3.1, 4.4, 3.9, 0.4, 4.2, 4.5, 4.9, 0.9};
    int data_count = sizeof(data) / sizeof(data[0]);

    //Setup bin info
    float min_meas = 0, max_meas = 5;
    int bin_count = 5;

    //Setup bins
    float bin_maxes[bin_count];
    int bin_counts[bin_count];
    //Initialize the array to 0;
    memset(bin_counts, 0, sizeof(bin_counts));

    float bin_width = (max_meas - min_meas) / bin_count;
    for (int b = 0; b < bin_count; b++) {
        bin_maxes[b] = min_meas + bin_width * (b + 1);
    }

    int thread_count = 4;
    //Setup an array of "local" bin counts for "message passing"
    int threaded_bin_cts[thread_count][bin_count];
    memset(threaded_bin_cts, 0, sizeof(threaded_bin_cts));
#   pragma omp parallel num_threads(thread_count)
    {
        //Bin the data for my chunk
        int chunk_size = data_count / omp_get_num_threads();
        int data_start = omp_get_thread_num() * chunk_size;
        for (int i = data_start; i < data_start + chunk_size; i++) {
            int bin = Find_bin(data[i], bin_maxes, bin_count, min_meas);
            if (bin >= 0) {
                threaded_bin_cts[omp_get_thread_num()][bin]++;
            }
        }
    }
    //Sum the "local" bin counts
    //Yes I could do parallel summing but decided to take the naive approach
    //Honestly this whole approach is crap (*cough cough modulo*), but hey it's how the book suggests
    for (int i = 0; i < thread_count; i++) {
        for (int k = 0; k < bin_count; k++) {
            bin_counts[k] += threaded_bin_cts[i][k];
        }
    }

    printf("%f - %f: %d\n", min_meas, bin_maxes[0], bin_counts[0]);
    for (int i = 1; i < bin_count; i++) {
        printf("%f - %f: %d\n", bin_maxes[i - 1], bin_maxes[i], bin_counts[i]);
    }

    return 0;
}

